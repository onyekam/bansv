<?php namespace Expressuals\Bansv\Components;

use Cms\Classes\ComponentBase;
use Expressuals\Bansv\Models\DieselSupply as DS;
use Expressuals\Bansv\Models\DieselStatus as DStat;
use Expressuals\Bansv\Models\GenTimelog as GenTL;
use Expressuals\Bansv\Models\NepaTimelog as NepaTL;
use Flash;
use Redirect;
use Input;
use Carbon\Carbon;

class DieselManagement extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'DieselManagement Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->genStatus = $this->getGenStatus();
        $this->nepaStatus = $this->getNepaStatus();
        $this->lastPurchase = $this->getLastDieselPurchase();
        $this->dieselQuantity = $this->getCurrentDieselStatus();
    }

    public function getLastDieselPurchase(){
        $lastPurchase = DS::orderBy('created_at', 'desc')->first();
        return $lastPurchase;
    }

    public function getCurrentDieselStatus(){
        $dieselQuantity = DStat::orderBy('created_at', 'desc')->first();
        return $dieselQuantity;
    }

    public function getGenStatus(){
        $genStatus = GenTL::orderBy('created_at', 'desc')->first();
        return $genStatus->gen_status;
    }

    public function getNepaStatus(){
        $nepaStatus = NepaTL::orderBy('created_at', 'desc')->first();
        return $nepaStatus->nepa_status;
    }

    public function getLastGenTimeLog(){
        $genLastLog = GenTL::orderBy('created_at', 'desc')->first();
        return $genLastLog;
    }

    public function getLastNepaTimeLog(){
        $nepaLastLog = NepaTL::orderBy('created_at', 'desc')->first();
        return $nepaLastLog;
    }

    public function onToggleGen(){
        $currentGenStatus = $this->getLastGenTimeLog();
        $genTL = new GenTL;
        $genTL->gen_status = post('gen_status');
        if($currentGenStatus->gen_status){
            $now = Carbon::now();
            $genTL->duration = $now->diff($currentGenStatus->created_at)->format('%H:%I:%S');
        }
        $genTL->save();
        return Redirect::to('light-switch');
    }

    public function onToggleNepa(){
        $currentNepaStatus = $this->getLastNepaTimeLog();
        $nepaTL = new NepaTL;
        $nepaTL->nepa_status = post('nepa_status');
        if($currentNepaStatus->gen_status){
            $now = Carbon::now();
            $genTL->duration = $now->diff($currentNepaStatus->created_at)->format('%H:%I:%S');
        }
        $nepaTL->save();
        return Redirect::to('light-switch');
    }

    public function onDieselSupply(){
        $ds = new DS;
        $ds->quantity = Input::get('quantity');
        $ds->save();
        Flash::success('Supplied Quantity Submitted');
        return Redirect::to('diesel-supply');

    }

    public function onDieselStatusUpdate(){
        $ds = new DStat;
        $ds->current_quantity = Input::get('quantity');
        $ds->save();
        Flash::success('Current Quantity Updated');
        return Redirect::to('diesel-status');
    }

    public $genStatus;
    public $nepaStatus;
    public $dieselQuantity;
    public $lastPurchase;
}
