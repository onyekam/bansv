<?php namespace Expressuals\Bansv;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Expressuals\Bansv\Components\DieselManagement' => 'bansv'
        ];
    }

    public function registerSettings()
    {
    }
}
