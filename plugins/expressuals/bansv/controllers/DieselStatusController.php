<?php namespace Expressuals\Bansv\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class DieselStatusController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'genadmin' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Expressuals.Bansv', 'main-menu-item');
    }
}
