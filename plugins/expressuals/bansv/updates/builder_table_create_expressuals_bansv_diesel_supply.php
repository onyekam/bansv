<?php namespace Expressuals\Bansv\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateExpressualsBansvDieselSupply extends Migration
{
    public function up()
    {
        Schema::create('expressuals_bansv_diesel_supply', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->decimal('quantity', 10, 0)->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('expressuals_bansv_diesel_supply');
    }
}
