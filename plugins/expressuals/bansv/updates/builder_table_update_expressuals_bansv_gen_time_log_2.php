<?php namespace Expressuals\Bansv\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateExpressualsBansvGenTimeLog2 extends Migration
{
    public function up()
    {
        Schema::table('expressuals_bansv_gen_time_log', function($table)
        {
            $table->string('duration')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('expressuals_bansv_gen_time_log', function($table)
        {
            $table->decimal('duration', 10, 0)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
