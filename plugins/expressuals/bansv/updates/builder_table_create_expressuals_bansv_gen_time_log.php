<?php namespace Expressuals\Bansv\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateExpressualsBansvGenTimeLog extends Migration
{
    public function up()
    {
        Schema::create('expressuals_bansv_gen_time_log', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->boolean('gen_status')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('expressuals_bansv_gen_time_log');
    }
}
