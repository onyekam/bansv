<?php namespace Expressuals\Bansv\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateExpressualsBansvNepaTimeLog extends Migration
{
    public function up()
    {
        Schema::table('expressuals_bansv_nepa_time_log', function($table)
        {
            $table->decimal('duration', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('expressuals_bansv_nepa_time_log', function($table)
        {
            $table->dropColumn('duration');
        });
    }
}
