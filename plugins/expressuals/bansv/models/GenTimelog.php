<?php namespace Expressuals\Bansv\Models;

use Model;

/**
 * Model
 */
class GenTimelog extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'expressuals_bansv_gen_time_log';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
