<?php namespace Expressuals\Bansv\Models;

use Model;

/**
 * Model
 */
class DieselSupply extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'expressuals_bansv_diesel_supply';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'user' => 'RainLab\User\Models\User',
    ];
}
