<?php namespace Expressuals\Bansv\Models;

use Model;

/**
 * Model
 */
class NepaTimelog extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'expressuals_bansv_nepa_time_log';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

}
